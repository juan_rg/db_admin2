using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DB_Admin.DataAccessLayer;
using DB_Admin.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DB_Admin.Controllers
{
    [Route("api/[controller]")]
    public class RequestsController : Controller
    {
        // GET api/requests
        [HttpGet]
        public string Get()
        {
            List<Request> lista = DALRequests.Req("SIN", null, null);
            var json = JsonConvert.SerializeObject(lista);
            json.Replace("null", "\"\"");
            return json;
        }
    }
}