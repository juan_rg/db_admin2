using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DB_Admin.Models;


namespace DB_Admin.DataAccessLayer
{
    public static class DALRequests
    {
        public static List<Request> Req(string css, DateTime? desde, DateTime? hasta)
        {
            List<Request> items;
            DateTime fecha;
            using (StreamReader r = new StreamReader("C:\\Users\\juan.rg\\documents\\visual studio 2017\\Projects\\DB_Admin\\DB_Admin\\Content\\JSON-datos.json"))
            {
                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<Request>>(json);
            }
            if (!string.IsNullOrEmpty(css) && !css.ToUpper().Equals("SIN"))
            {
                items = items.Where(x => x.Css.ToUpper().Equals(css.ToUpper())).ToList();
            }
            if (desde != null)
            {
                items = items.Where(x => DateTime.TryParse(x.Date, out fecha) && fecha >= desde.Value).ToList();
            }
            if (hasta != null)
            {
                items = items.Where(x => DateTime.TryParse(x.Date, out fecha) && fecha <= hasta.Value).ToList();
            }
            return items;
        }

        public static Dictionary<string, string> Csss()
        {
            Dictionary<string, string> diccionarioCss = new Dictionary<string, string>();
            diccionarioCss.Add("AR", "AR");
            diccionarioCss.Add("CARIBBEAN", "CARIBBEAN");
            diccionarioCss.Add("CL", "CL");
            diccionarioCss.Add("CO", "CO");
            diccionarioCss.Add("EC", "EC");
            diccionarioCss.Add("PE", "PE");
            diccionarioCss.Add("PR", "PR");
            diccionarioCss.Add("UY", "UY");
            diccionarioCss.Add("VE", "VE");
            return diccionarioCss;
        }
    }
}