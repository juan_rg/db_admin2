using System;

namespace DB_Admin.Models
{
    public class Request
    {
        public string ID= "";
        public string ClientNumber= "";
        public string FullName= "";
        public string Email= "";
        public string Telephone= "";
        public string Cellphone= "";
        public string Comments= "";
        public string Date= "";
        public string URLFrom= "";
        public string IsClient= "";
        public string FormURL= "";
        public string Css= "";
        public string ExtraColumn= "";
        public string ContactReasonID= "";
        public string ContactReasonDescription= "";
        public string SiteFirstPage= "";
        public string UtmSource= "";
    }
}